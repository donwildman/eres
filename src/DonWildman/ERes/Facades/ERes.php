<?php namespace DonWildman\ERes\Facades;

use Illuminate\Support\Facades\Facade;

class ERes extends Facade {

    protected static function getFacadeAccessor() { return 'eres'; }

}
