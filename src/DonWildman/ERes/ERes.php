<?php namespace DonWildman\ERes;

require_once('Base_ERes.php');

class ERes extends BaseERes {
	
	// **************** eRes ******************************

    public function availability($params=array()){

        return $this->HotelAvailRQ($params);
    }

    public function retrieve($params=array())
    {
        return $this->HotelReadRQ_Retrieve($params);
    }

	public function book($params=array())
	{

		return $this->HotelResRQBook($params);

	}

	public function confirm($params=array())
	{
		return $this->HotelResRQ_Confirm($params);
	}
	
	public function cancel($params=array())
	{
		return $this->CancelRQ($params);
	}
	
	public function search($params=array())
	{
		return $this->SearchRQ($params);
	}


	// **************** GroupView ******************************

	public function login($params=array())
	{
		return $this->ReadRQ($params);
	}

	public function bookingHistory($params=array())
	{
		return $this->ReadRQ_History($params);
	}
}