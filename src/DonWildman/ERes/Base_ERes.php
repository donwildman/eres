<?php namespace DonWildman\ERes;

use Guzzle\Http\Client;
use Guzzle\Common\Event;
use \SimpleXMLElement;
use \Session;


class BaseERes
{

	private $eResUrl = 'http://196.25.20.108:8088/APIWebModule_v5/APIServlet';
	private $groupViewUrl = 'http://196.25.20.105:8080/groupView/seam/resource/api';
	private $eResTestUrl = 'http://196.25.159.179:8087';
	private $groupViewTestUrl = 'http://196.25.159.181:8120';

	private $client;

// ********************************************************* eRes *************************************************

    public function __construct()
    {
		
    }

	public function addRequestors($xml_file, $params)
	{
		// corporate booking
		if($params['booking_type'] == 'corporate') {
			$source = $xml_file->POS->addChild('Source');
			$requestor = $source->addChild('RequestorID');
			$requestor->addAttribute('Type', '4');
			$requestor->addAttribute('ID', $params['corporate_id']);
			$requestor->addAttribute('Instance', '0123456');
			if($params['redemption'] == 'true') {
				$source1 = $xml_file->POS->addChild('Source');
				$requestor1 = $source1->addChild('RequestorID');
				$requestor1->addAttribute('Type', '5');
				$requestor1->addAttribute('ID', '19181');
				$requestor1->addAttribute('Instance', '0123456');
				$source2 = $xml_file->POS->addChild('Source');
				$requestor2 = $source2->addChild('RequestorID');
				$requestor2->addAttribute('Type', '22');
				$requestor2->addAttribute('ID', '44');
				$requestor2->addAttribute('Instance', '0123456');
			} else {
				$source1 = $xml_file->POS->addChild('Source');
				$requestor = $source1->addChild('RequestorID');
				$requestor->addAttribute('Type', '5');
				$requestor->addAttribute('ID', '16537');
				$requestor->addAttribute('Instance', '0123456');
				$source2 = $xml_file->POS->addChild('Source');
				$requestor2 = $source2->addChild('RequestorID');
				$requestor2->addAttribute('Type', '22');
				$requestor2->addAttribute('ID', '30');
				$requestor2->addAttribute('Instance', '0123456');
			}
		}
		// lodger club booking
		if($params['booking_type'] == 'lodger') {
			if($params['redemption'] == 'true') {
				$source1 = $xml_file->POS->addChild('Source');
				$requestor1 = $source1->addChild('RequestorID');
				$requestor1->addAttribute('Type', '5');
				$requestor1->addAttribute('ID', '19181');
				$requestor1->addAttribute('Instance', '0123456');
				$source2 = $xml_file->POS->addChild('Source');
				$requestor2 = $source2->addChild('RequestorID');
				$requestor2->addAttribute('Type', '22');
				$requestor2->addAttribute('ID', '44');
				$requestor2->addAttribute('Instance', '0123456');
			} else {
				$source1 = $xml_file->POS->addChild('Source');
				$requestor1 = $source1->addChild('RequestorID');
				$requestor1->addAttribute('Type', '5');
				$requestor1->addAttribute('ID', '1248637');
				$requestor1->addAttribute('Instance', '0123456');
				$source2 = $xml_file->POS->addChild('Source');
				$requestor2 = $source2->addChild('RequestorID');
				$requestor2->addAttribute('Type', '22');
				$requestor2->addAttribute('ID', '30');
				$requestor2->addAttribute('Instance', '0123456');
			}
			$xml_file->AvailRequestSegments->AvailRequestSegment->Profiles->ProfileInfo->Profile->Customer->CustLoyalty->addAttribute('MembershipID', $params['membership_id']);
		} else {
			// express booking
			$source1 = $xml_file->POS->addChild('Source');
			$requestor1 = $source1->addChild('RequestorID');
		
			$requestor1->addAttribute('Type','5');
			$requestor1->addAttribute('ID', '1248444');
			$requestor1->addAttribute('Instance', '0123456');

			$source2 = $xml_file->POS->addChild('Source');
			$requestor2 = $source2->addChild('RequestorID');
			$requestor2->addAttribute('Type', '22');
			$requestor2->addAttribute('ID', '60');
			$requestor2->addAttribute('Instance', '0123456');
		}

		return $xml_file;
	}

	public function HotelAvailRQ($params){

        $xml = simplexml_load_file('Messages/eRes/OTA_HotelAvailRQ.xml');

		$xml_file = $this->addRequestors($xml, $params);

        $xml_file->AvailRequestSegments->AvailRequestSegment->StayDateRange['Start'] = date("Y-m-d",strtotime($params['from_date']));
        $xml_file->AvailRequestSegments->AvailRequestSegment->StayDateRange['End'] = date("Y-m-d",strtotime($params['to_date']));

//		$xml_file->AvailRequestSegments->AvailRequestSegment->Profiles->ProfileInfo->Profile->Customer->CustLoyalty->addAttribute('MembershipID', $params['membership_id']);

        $xml_file->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->GuestCounts->GuestCount['Count'] = $params['adults'];

        $xml_file->AvailRequestSegments->AvailRequestSegment->HotelSearchCriteria->Criterion->HotelRef['HotelCode'] = $params['hotel_id'];

		$xmlMessage = $xml_file->asXML();

		$this->client = new Client($this->eResTestUrl);
		
		
		$request = $this->client->post('/APIWebModule_v5/APIServlet');
		$request->setBody($xmlMessage);
		$request->setHeader('Content-Type', 'text/xml');
		try{
			$data = $request->send();
			return $data->getBody(true);
		}
		catch (Guzzle\Http\Exception\BadResponseException $e) {
			
			return $e->getResponse();
			
		}
		catch (Guzzle\Http\Exception\ServerErrorResponseException $e) {
			
			return $e->getResponse();
		}

		

	}

    public function HotelReadRQ_Retrieve($params){

		$xml_file = simplexml_load_file('Messages/eRes/OTA_ReadRQ_Retrieve.xml');

		$source = $xml_file->POS->addChild('Source');
		$requestor2 = $source->addChild('RequestorID');
		$requestor2->addAttribute('Type','22');
		$requestor2->addAttribute('ID','57');
		$requestor2->addAttribute('Instance', '0123456');

		$xml_file->ReadRequests->addChild('ReadRequest');


		$user = $xml_file->ReadRequests->ReadRequest->UniqueID['ID'] = $params['booking_id'];
//		$user->addAttribute('Type', '14');
//		$user->addAttribute('ID', $params['booking_id']);

		$xml_file->ReadRequests->ReadRequest->addChild('Verification');

//		if(!empty($params['email']))
//		{
			$source = $xml_file->ReadRequests->ReadRequest->Verification;
			$email = $source->addChild('Email', $params['email']);
//		}

//		if(!empty($params['telephone']))
//		{
//			$source = $xml_file->ReadRequests->ReadRequest->Verification;
//			$email = $source->addChild('TelephoneInfo');
//			$email->addAttribute('PhoneNumber', $params['telephone']);
//		}

		
        $xmlMessage = $xml_file->asXML();

		$this->client = new Client($this->eResTestUrl);
		
        $request = $this->client->post('/APIWebModule_v5/APIServlet');
        $request->setBody($xmlMessage);
        $request->setHeader('Content-Type', 'text/xml');
		
		try {
			$data = $request->send();
			$result = $data->getBody(true);
			
			if(isset($result->ErrorCode)){
				return ErrorMessage;
			}
			return $result;
		}
		catch (Guzzle\Http\Exception\BadResponseException $e) {
			return $e->getResponse();
		}
		catch (Guzzle\Http\Exception\ServerErrorResponseException $e) {
			return $e->getResponse();
		}
    }

	public function HotelResRqBook($params) 
	{
		$xml = simplexml_load_file('Messages/eRes/OTA_HotelResRQ.xml');

		$xml_file = $this->addRequestors($xml, $params);

		$uniqueID = $xml_file->HotelReservations->HotelReservation->UniqueID;
		$uniqueID->addAttribute('ID', $params['id']);
		$roomType = $xml_file->HotelReservations->HotelReservation->RoomStays->RoomStay->RoomTypes->RoomType;
		$roomType['RoomTypeCode'] = $params['room_type_code'];
		$roomType['NonSmoking'] = (substr($params['room_type_code'],0,2) == 'NS') ? 'true' : 'false';

		$roomRate = $xml_file->HotelReservations->HotelReservation->RoomStays->RoomStay->RoomRates->RoomRate;
		$roomRate['RoomTypeCode'] = $params['room_type_code'];

		$guestCount = $xml_file->HotelReservations->HotelReservation->RoomStays->RoomStay->GuestCounts->GuestCount;
		$guestCount['Count'] = 2;

		$timeSpan = $xml_file->HotelReservations->HotelReservation->RoomStays->RoomStay->TimeSpan;
		$timeSpan['Start'] = date('Y-m-d', strtotime($params['from_date']));
		$timeSpan['End'] = date('Y-m-d', strtotime($params['to_date']));

		$basicPropertyInfo = $xml_file->HotelReservations->HotelReservation->RoomStays->RoomStay->BasicPropertyInfo;
		$basicPropertyInfo['HotelCode'] = $params['hotel_id'];

		$xmlMessage = $xml_file->asXML();

		$this->client = new Client($this->eResTestUrl);
		
		$request = $this->client->post('/APIWebModule_v5/APIServlet');
		$request->setBody($xmlMessage);
		$request->setHeader('Content-Type', 'text/xml');
		try {
			$data = $request->send();
			return $data->getBody(true);
		}	
		catch (Guzzle\Http\Exception\BadResponseException $e) {
			
			return $e->getResponse();
		}
		catch (Guzzle\Http\Exception\ServerErrorResponseException $e) {
			
			return $e->getResponse();
		}
	}

	
	
	public function HotelResRQ_Confirm($params)
	{
		$xml = simplexml_load_file('Messages/eRes/OTA_HotelResRQ_Confirm.xml');

		$xml_file = $this->addRequestors($xml, $params);
		
		\Log::info('params = ' . json_encode($params));
		$uniqueID = $xml_file->HotelReservations->HotelReservation->UniqueID;
		$uniqueID->addAttribute('ID', $params['id']);
	
		$roomType = $xml_file->HotelReservations->HotelReservation->RoomStays->RoomStay->RoomTypes->RoomType;
		$roomType['RoomTypeCode'] = $params['room_type_code'];
		$roomType['NonSmoking'] = (substr($params['room_type_code'],0,2) == 'NS') ? 'true' : 'false';

		$roomRate = $xml_file->HotelReservations->HotelReservation->RoomStays->RoomStay->RoomRates->RoomRate;
		$roomRate['RoomTypeCode'] = $params['room_type_code'];

		$guestCount = $xml_file->HotelReservations->HotelReservation->RoomStays->RoomStay->GuestCounts->GuestCount;
		$guestCount['Count'] = 2;

		$timeSpan = $xml_file->HotelReservations->HotelReservation->RoomStays->RoomStay->TimeSpan;
		$timeSpan['Start'] = date('Y-m-d', strtotime($params['from_date']));
		$timeSpan['End'] = date('Y-m-d', strtotime($params['to_date']));

		$basicPropertyInfo = $xml_file->HotelReservations->HotelReservation->RoomStays->RoomStay->BasicPropertyInfo;
		$basicPropertyInfo['HotelCode'] = $params['hotel_id'];

		// Guest details
		$customer = $xml_file->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->Profile->Customer;
		
		$customer->PersonName->NamePrefix = $params['guest_title'];
		$customer->PersonName->GivenName = $params['guest_first_name'];
		$customer->PersonName->Surname = $params['guest_last_name'];
		$customer->Telephone[0]['PhoneNumber'] = $params['guest_tel'];
		$customer->Telephone[1]['PhoneNumber'] = $params['guest_cell'];
		$customer->Email = $params['guest_email'];
		$customer->Address->AddressLine[0] = $params['guest_address1'];
//		$customer->Address->AddressLine[1] = $params['guest_address2'];
		$customer->Address->CityName = $params['guest_city'];
		$customer->Address->PostalCode = $params['guest_postal_code'];
		$customer->Address->StateProv['StateCode'] = $params['guest_province'];
		$customer->Address->CountryName = $params['guest_country'];
		$customer->ContactPerson->PersonName->NamePrefix = $params['guest_title'];
		$customer->ContactPerson->PersonName->GivenName = $params['guest_first_name'];
		$customer->ContactPerson->PersonName->Surname = $params['guest_last_name'];
//		$xml_file->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->Profile->Customer->Document['DocID'] = $params['guest_id_no'];

//		if($params['booking_type'] != 'express'){
//			$customer->CustLoyalty->addAttribute('MembershipID', $params['membership_id']);
//		}

		if(!empty($params['comments'])){
			$xml_file->HotelReservations->HotelReservation->ResGlobalInfo->Comments->Comment[0]->Text = $params['comments'];
		}
		if(!empty($params['comments_hidden'])) {
			$xml_file->HotelReservations->HotelReservation->ResGlobalInfo->Comments->Comment[1]->Text = $params['comments_hidden'];
		}
		$xmlMessage = $xml_file->asXML();
		\Log::info('confirm message = ' . $xmlMessage);

		$this->client = new Client($this->eResTestUrl);
		$request = $this->client->post('/APIWebModule_v5/APIServlet');
		$request->setBody($xmlMessage);
		$request->setHeader('Content-Type', 'text/xml');
		
		try {
			$data = $request->send();
			return $data->getBody(true);
			
		} 
		catch (Guzzle\Http\Exception\BadResponseException $e) {
		
			return $e->getResponse();
		}
		catch (Guzzle\Http\Exception\ServerErrorResponseException $e) {
		
			return $e->getResponse();
		}

	}
	
	public function CancelRQ($params)
	{
		$xml = simplexml_load_file('Messages/eRes/OTA_HotelResRQ_Confirm.xml');

		$xml_file = $this->addRequestors($xml, $params);

		$xml_file->UniqueID['ID'] = $params['booking_id'];
		
		$xmlMessage = $xml_file->asXML();
		
		$request = $this->client->post('/APIWebModule_v5/APIServlet');
		$request->setBody($xmlMessage);
		$request->setHeader('Content-Type', 'text/xml');
		$data = $request->send();
		return $data->getBody(true);

	}

	public function SearchRQ($params)
	{
		$xml = simplexml_load_file('Messages/eRes/OTA_ReadRQ.xml');

		$xml_file = $this->addRequestors($xml, $params);

		$xml_file->ReadRequests->HotelReadRequest->Verification->CustLoyalty['MembershipID'] = $params['username'];
		$xml_file->ReadRequests->HotelReadRequest->SelectionCriteria['Start'] = $params['from_date'];
		$xml_file->ReadRequests->HotelReadRequest->SelectionCriteria['End'] = $params['to_date'];

		$xmlMessage = $xml_file->asXML();

		$request = $this->client->post('/APIWebModule_v5/APIServlet');
		$request->setBody($xmlMessage);
		$request->setHeader('Content-Type', 'text/xml');
		$data = $request->send();
		return $data->getBody(true);

		//			$hotelReadRequest = $xml_file->ReadRequests->addChild('HotelReadRequest');
////			$hotelReadRequest->addAttribute('HotelCode', $params['hotel_id']);
//
//			$verification = $hotelReadRequest->addChild('Verification');
//
//			$custLoyalty = $verification->addChild('CustLoyalty');
//			$custLoyalty->addAttribute('MembershipID', $params['membership_id']);
//
//			$dates = $xml_file->ReadRequests->HotelReadRequest->addChild('SelectionCriteria');
//			$dates->addAttribute('Start', $params['from_date']);
//			$dates->addAttribute('End', $params['to_date']);
//			$dates->addAttribute('DateType', 'DepartureDate');
	}
	
	

	// ************************************************ GroupView *************************************************
	
	
	public function ReadRQ($params)
	{
//		dd($params);
		$xml_file = simplexml_load_file('Messages/GroupView/OTA_ReadRQ_Account.xml');

		$userID = $xml_file->ReadRequests->HotelReadRequest->UserID;
		$userID->addAttribute('ID', $params['username']);
		$userID->addAttribute('PinNumber', $params['password']);

		$xmlMessage = $xml_file->asXML();
		
		$this->client = new Client($this->groupViewUrl);
		
		$request = $this->client->post('/groupView/seam/resource/api');
		$request->setBody($xmlMessage);
		$request->setHeader('Content-Type', 'text/xml');

		$data = $request->send();
		return $data->getBody(true);
	}

	public function ReadRQ_History($params)
	{
		$xml_file = simplexml_load_file('Messages/GroupView/OTA_ReadlRQ_History.xml');

		$xml_file->ReadRequests->HotelReadRequest->UserID['ID'] = $params['username'];
		
		$xml_file->ReadRequests->HotelReadRequest->SelectionCriteria['Start'] = date('Y-m-d', strtotime($params['from_date']));;
		$xml_file->ReadRequests->HotelReadRequest->SelectionCriteria['End'] = date('Y-m-d', strtotime($params['to_date']));;

		$xmlMessage = $xml_file->asXML();

		$this->client = new Client($this->groupViewUrl);

		$request = $this->client->post('/groupView/seam/resource/api');
		$request->setBody($xmlMessage);
		$request->setHeader('Content-Type', 'text/xml');

		$data = $request->send();
		return $data->getBody(true);
	}
	
}